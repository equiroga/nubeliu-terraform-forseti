project_id = "terraform-forseti"
org_id     = "1234567890"
domain     = "nubeliu.com"
region     = "us-east1"

network         = "forseti"
subnetwork      = "forseti-subnet1"
network_project = ""

gsuite_admin_email      = "admin@nubeliu.com"
sendgrid_api_key        = ""
forseti_email_sender    = ""
forseti_email_recipient = ""
